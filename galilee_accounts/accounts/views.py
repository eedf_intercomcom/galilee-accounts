# -*- coding: utf-8 -*-
###############################################################################
#       accounts/views.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2020-2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-accounts.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Vues de l'application de gestion des comptes utilisateurices de Galilée"""

import sys
import uuid
import smtplib
import datetime
import logging

from django.template.loader import render_to_string
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from .forms import CreationForm, CreationFormExt, ChMotDePasse, FiltresForm
from .models import AdhValidation, ExtValidation, GuestLastNumAdh, MdpValidation
from django.conf import settings

sys.path.insert(0, settings.GALILEE_LDAP_PATH)
import galilee_seafile_ldap
from galilee_seafile_ldap import NotFound

SEAF_OIDC_ENABLED = getattr(galilee_seafile_ldap.conf, 'OICD_REMPLACE_LDAP', False)

# Get an instance of a logger
#logger = logging.getLogger(__name__)
logger = logging.getLogger('galilee-accounts')

def creation(request):
    """Vue de création de compte - adhérent·es"""
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = CreationForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # s'il y a une/des anciennes demandes, les supprimer.
            ancienne_demande = False
            ancienne_demande_list = AdhValidation.objects.filter(
                numadh = form.cleaned_data['numadh']
            )
            for ancienne_demande_o in ancienne_demande_list:
                ancienne_demande_o.delete()
                ancienne_demande = True
            
            gldap = settings.LDAP_CONNECTOR
            # récupération du mail
            adresse_mail = gldap.adh_data(form.cleaned_data['numadh'], ['mail'])['mail'][0]
            
            # Créer un nouvel objet Django
            valid = AdhValidation()
            valid.numadh = form.cleaned_data['numadh']
            valid.login = form.cleaned_data['login']
            # we hash the password before saving it
            valid.motdepasse = gldap.make_secret(form.cleaned_data['motdepasse'])
            valid.confirmation_code = str(uuid.uuid4())
            valid.adresse_mail = adresse_mail
            valid.save()
            logger.info("Ajout en bdd d'une validation adh :" + 
                valid.numadh + ' ' + valid.login + ' ' + valid.adresse_mail)
            
            # envoi du mail
            mail_contenu = render_to_string('accounts/creation_adh_mail.txt', {
                'intercomcom_mail': settings.INTERCOMCOM_MAIL,
                'confirm_url': request.build_absolute_uri(reverse(adh_confirm, args=[valid.confirmation_code])),
                'ancienne_demande': ancienne_demande,
                'SEAF_OIDC_ENABLED': SEAF_OIDC_ENABLED,
            })
            sujet = "[EEDF Galilée] Confirmation de la création de votre compte sur Galilée"

            if settings.DEBUG_PRINT_MAIL:
                print('To: {}'.format(adresse_mail))
                print(mail_contenu)
            else:
                try:
                    _sendmail(settings.INTERCOMCOM_MAIL, adresse_mail, sujet, mail_contenu)
                except smtplib.SMTPRecipientsRefused:
                    # adresse invalide dans le portail
                    logger.info("Erreur d'envoi de mail, adresse invalide : " +
                        adresse_mail + ' (' + valid.login + ')')
                    return HttpResponseRedirect(reverse(mail_invalide, args=[adresse_mail]))
            
            # build the obsfucated mail
            mail_user, mail_domain = adresse_mail.split('@')
            mail_user = mail_user[0:2] + ('*' * (len(mail_user) - 3))  + mail_user[-1:]
            obs_mail = mail_user + '@' + mail_domain
            logger.info("Envoi du mail de validation adh à " + adresse_mail +
                ' (' + valid.login + ')')
            
            return HttpResponseRedirect(reverse(demande_en_cours, args=[obs_mail]))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = CreationForm()    
    
    structures_data = [
        settings.LDAP_CONF.STRUCTURES[s][1] 
        for s in settings.LDAP_CONF.STRUCTURES
        if s != settings.LDAP_CONF.GUEST_OU
    ]
    structure_list = ', '.join(structures_data[:-1]) + ' et ' + structures_data[-1]
    
    return render(request, "accounts/creation.html", {
        'request': request,
        'form': form,
        'structure_list': structure_list,
    })

def demande_en_cours(request, obs_mail):
    """Vue d'une demande en cours d'adhérent·e"""
    return render(request, "accounts/demande_en_cours.html", {
        'obs_mail': obs_mail,
    })

def mail_invalide(request, adresse_mail):
    """Vue d'erreur en cas de mail invalide"""
    return render(request, "accounts/mail_invalide.html", {
        'mail': adresse_mail,
    })
    
def adh_confirm(request, code):
    """Confirmation d'une demande d'adhérent·e -> création du compte"""
    # vérifier l'existence de la demande
    try:
        adh_confirm = AdhValidation.objects.get(confirmation_code=code)
    except AdhValidation.DoesNotExist:
        return render(request, "accounts/demande_introuvable.html", {
            'creation': True})
    
    logger.info("Adh_confirm pour " + adh_confirm.adresse_mail +
        ' (' + adh_confirm.login + ')')
    # créer le compte LDAP
    gldap = settings.LDAP_CONNECTOR
    gldap.creer_compte(
        adh_confirm.numadh,
        adh_confirm.login,
        adh_confirm.motdepasse,
        hashed = True,
    )
    
    login = adh_confirm.login
    adresse_mail = adh_confirm.adresse_mail
    numadh = adh_confirm.numadh
    
    # suppression de la demande
    adh_confirm.delete()
    
    # ajout automatique aux groupes Seafile
    if settings.AJOUT_AUTO_GRP_SEAFILE:
        sg = galilee_seafile_ldap.SeafileGroupes()
        try: 
            sg.ajouter_groupes_par_defaut(numadh)
        except Exception as err:
            logger.error(str(err))            
    
    # envoi d'un mail de confirmation à la personne
    mail_contenu = render_to_string('accounts/confirmation_mail.txt', {
        'intercomcom_mail': settings.INTERCOMCOM_MAIL,
        'login': login,
        'mail': adresse_mail,
        'SEAF_OIDC_ENABLED': SEAF_OIDC_ENABLED,
    })
    sujet = "[EEDF Galilée] Ton compte Galilée vient d'être validé !"
        
    if settings.DEBUG_PRINT_MAIL:
        print('To: {}'.format(adresse_mail))
        print(mail_contenu)
    else:
        try:
            _sendmail(settings.INTERCOMCOM_MAIL, adresse_mail, sujet, mail_contenu)
        except smtplib.SMTPRecipientsRefused:                            
            # adresse invalide dans le portail                           
            logger.info("Erreur d'envoi de mail, adresse invalide : " +  
                    adresse_mail + ' (' + valid.login + ')')                 
            return HttpResponseRedirect(reverse(mail_invalide, args=[adresse_mail]))
    
    return render(request, "accounts/adh_confirm.html", {
        'login': login,
        'mail': adresse_mail,
        'SEAF_OIDC_ENABLED': SEAF_OIDC_ENABLED,
    })

def creation_ext(request):
    """Vue de création de compte - personnes extérieures"""
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = CreationFormExt(request.POST)
        # check whether it's valid:
        if form.is_valid():

            # s'il y a une/des anciennes demandes, les supprimer.
            ancienne_demande = False
            ancienne_demande_list = ExtValidation.objects.filter(
                adresse_mail = form.cleaned_data['adresse_mail']
            )
            for ancienne_demande_o in ancienne_demande_list:
                ancienne_demande_o.delete()
                ancienne_demande = True
            
            # Créer un nouvel objet Django
            gldap = settings.LDAP_CONNECTOR
            valid = ExtValidation()
            valid.nom = form.cleaned_data['nom']
            valid.prenom = form.cleaned_data['prenom']
            valid.fonction = form.cleaned_data['fonction']
            valid.adresse_mail = form.cleaned_data['adresse_mail']
            valid.login = form.cleaned_data['login']
            # we hash the password before saving it
            valid.motdepasse = gldap.make_secret(form.cleaned_data['motdepasse'])
            valid.motivation = form.cleaned_data['motivation']
            valid.confirmation_code = str(uuid.uuid4())
            valid.save()
            logger.info("Ajout en bdd d'une validation ext : " + 
                valid.login + ' ' + valid.adresse_mail)
            
            # envoi du mail à l'intercomcom
            mail_contenu = render_to_string('accounts/creation_ext_mail.txt', {
                'intercomcom_mail': settings.INTERCOMCOM_MAIL,
                'prenom': valid.prenom,
                'nom': valid.nom,
                'fonction': valid.fonction,
                'adresse_mail': valid.adresse_mail,
                'login': valid.login,
                'motivation': valid.motivation,
                'confirm_url': request.build_absolute_uri(reverse(ext_confirm, args=[valid.confirmation_code])),
                'refus_url': request.build_absolute_uri(reverse(ext_refus, args=[valid.confirmation_code])),
                'ancienne_demande': ancienne_demande,
            })
            sujet = "[EEDF Galilée] Demande de création de compte pour {prenom} {nom}".format(**{
                'prenom': valid.prenom,
                'nom': valid.nom
            })
            
            if settings.DEBUG_PRINT_MAIL:
                print('To: {}'.format(settings.INTERCOMCOM_MAIL))
                print(mail_contenu)
            else:
                _sendmail(settings.INTERCOMCOM_MAIL, settings.INTERCOMCOM_MAIL, sujet, mail_contenu)
            logger.info("Envoi du mail de validation ext à l'intercomcom pour "
                + valid.adresse_mail + ' (' + valid.login + ')')
            
            return HttpResponseRedirect(reverse(demande_en_cours_ext))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = CreationFormExt()    
    
    structures_data = [
        settings.LDAP_CONF.STRUCTURES[s][1] 
        for s in settings.LDAP_CONF.STRUCTURES
    ]
    structure_list = ', '.join(structures_data[:-1]) + ' et ' + structures_data[-1]
    
    return render(request, "accounts/creation_ext.html", {
        'request': request,
        'form': form,
        'structure_list': structure_list,
    })

def demande_en_cours_ext(request):
    """Vue du message indiquant qu'une demande est en cours."""
    return render(request, "accounts/demande_en_cours_ext.html", {
        'intercomcom_mail': settings.INTERCOMCOM_MAIL,
    })

def ext_confirm(request, code):
    """Confirmation d'une demande d'extérieur·e -> création du compte"""
    # vérifier l'existence de la demande
    ext_confirm = get_object_or_404(ExtValidation, confirmation_code=code)
    
    logger.info("Ext_confirm pour " + ext_confirm.login)

    # récupérer le dernier numéro d'adh factice
    try:
        last_numadh = GuestLastNumAdh.objects.all()[0]
    except IndexError:
        last_numadh = GuestLastNumAdh()
        last_numadh.save()
    
    gldap = settings.LDAP_CONNECTOR
    
    # vérifier si le numéro d'adh fictif n'est pas déjà utilisé. sinon l'incrémenter
    numadh_nonfree = True
    while numadh_nonfree:
        res = gldap.recherche_numadh('(cn={})'.format(last_numadh.numadh + 1))
        if res:
            # déjà utilisé
            last_numadh.numadh = last_numadh.numadh + 1
            last_numadh.save()
        else:
            numadh_nonfree = False
    
    numadh = str(last_numadh.numadh + 1)
    # créer le compte LDAP
    gldap.creer_compte_ext(
        ext_confirm.nom,
        ext_confirm.prenom,
        ext_confirm.fonction,
        numadh,
        ext_confirm.adresse_mail,
        ext_confirm.login,
        ext_confirm.motdepasse,
        hashed = True,
    )
    
    # incrémentation de last_numadh
    last_numadh.numadh = last_numadh.numadh + 1
    last_numadh.save()
    
    login = ext_confirm.login
    adresse_mail = ext_confirm.adresse_mail
    
    # suppression de la demande
    ext_confirm.delete()
    
    # ajout automatique aux groupes Seafile
    if settings.AJOUT_AUTO_GRP_SEAFILE:
        sg = galilee_seafile_ldap.SeafileGroupes()
        try:
            sg.ajouter_groupes_par_defaut(numadh)
        except Exception as err:
            logger.error(str(err))
    
    
    # envoi d'un mail de confirmation à la personne
    mail_contenu = render_to_string('accounts/confirmation_mail.txt', {
        'intercomcom_mail': settings.INTERCOMCOM_MAIL,
        'login': login,
        'mail': adresse_mail,
        'SEAF_OIDC_ENABLED': SEAF_OIDC_ENABLED,
    })
    sujet = "[EEDF Galilée] Ton compte Galilée vient d'être validé !"
        
    if settings.DEBUG_PRINT_MAIL:
        print('To: {}'.format(adresse_mail))
        print(mail_contenu)
    else:
        _sendmail(settings.INTERCOMCOM_MAIL, adresse_mail, sujet, mail_contenu)
    
    return render(request, "accounts/ext_confirm.html", {
        'login': login,
        'adresse_mail': adresse_mail,
    })

def ext_refus(request, code):
    """Vue de refus d'une demande de création de compte"""
    # vérifier l'existence de la demande
    ext_confirm = get_object_or_404(ExtValidation, confirmation_code=code)
    
    logger.info("Ext_refus pour " + ext_confirm.login)

    nom = ext_confirm.nom
    prenom = ext_confirm.prenom
    adresse_mail = ext_confirm.adresse_mail
    
    # suppression de la demande
    ext_confirm.delete()
    
    return render(request, "accounts/ext_refus.html", {
        'nom': nom,
        'prenom': prenom,
        'mail': adresse_mail,
    })

def changer_mdp(request):
    """Vue de changement de mot de passe"""
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ChMotDePasse(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # s'il y a une/des anciennes demandes, les supprimer.
            ancienne_demande = False
            ancienne_demande_list = MdpValidation.objects.filter(
                adresse_mail = form.cleaned_data['adresse_mail']
            )
            for ancienne_demande_o in ancienne_demande_list:
                ancienne_demande_o.delete()
                ancienne_demande = True
            
            # Créer un nouvel objet Django
            gldap = settings.LDAP_CONNECTOR
            valid = MdpValidation()
            valid.adresse_mail = form.cleaned_data['adresse_mail']
            valid.numadh = form.cleaned_data['numadh']
            # we hash the password before saving it
            valid.motdepasse = gldap.make_secret(form.cleaned_data['motdepasse'])
            valid.confirmation_code = str(uuid.uuid4())
            valid.save()
            
            # envoi du mail à la personne
            mail_contenu = render_to_string('accounts/changer_mdp.txt', {
                'intercomcom_mail': settings.INTERCOMCOM_MAIL,
                'adresse_mail': valid.adresse_mail,
                'confirm_url': request.build_absolute_uri(reverse(mdp_confirm, args=[valid.confirmation_code])),
                'ancienne_demande': ancienne_demande,
            })
            sujet = "[EEDF Galilée] Confirmation de votre changement de mot de passe"
            
            if settings.DEBUG_PRINT_MAIL:
                print('To: {}'.format(valid.adresse_mail))
                print(mail_contenu)
            else:
                try:
                    _sendmail(settings.INTERCOMCOM_MAIL, valid.adresse_mail, sujet, mail_contenu)
                except smtplib.SMTPRecipientsRefused:                            
                    # adresse invalide dans le portail                           
                    logger.info("Erreur d'envoi de mail, adresse invalide : " +  
                            adresse_mail + ' (' + valid.login + ')')                 
                    return HttpResponseRedirect(reverse(mail_invalide, args=[adresse_mail]))
            
            logger.info("Mail de changement de mdp envoyé à " + valid.adresse_mail)

            return HttpResponseRedirect(reverse(demande_en_cours_mdp))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ChMotDePasse()    
        
    return render(request, "accounts/changer_mdp.html", {
        'request': request,
        'form': form,
    })


def demande_en_cours_mdp(request):
    """Vue indiquant que la demande de changement de mot de passe est en cours"""
    return render(request, "accounts/demande_en_cours_mpd.html", {
        'intercomcom_mail': settings.INTERCOMCOM_MAIL,
    })
    
def mdp_confirm(request, code):
    """Confirmation d'une demande de changement de mot de passe"""
    # vérifier l'existence de la demande
    try:
        mdp_valid = MdpValidation.objects.get(confirmation_code=code)
    except MdpValidation.DoesNotExist:
        return render(request, "accounts/demande_introuvable.html", {
            'creation': False })
    
    # Modifier le mdp (déjà hashé)
    gldap = settings.LDAP_CONNECTOR
    gldap.editer_adh(mdp_valid.numadh, [('userPassword', mdp_valid.motdepasse)])
    
    # recupérer le login & le mail
    uid_data = gldap.adh_data(mdp_valid.numadh, ['uid'])
    if not uid_data:
        login = '' # ne devrait *vraiment* pas arriver…
    else:
        login = uid_data['uid'][0]
    adresse_mail = mdp_valid.adresse_mail 
    
    # suppression de la demande
    mdp_valid.delete()
    
    logger.info("Changement de mdp validé pour " + adresse_mail)

    return render(request, "accounts/mdp_confirm.html", {
        'login': login,
        'mail': adresse_mail,
        'SEAF_OIDC_ENABLED': SEAF_OIDC_ENABLED,
    })


def _sendmail(mail_from, mail_to, subject, content):
    """Envoi d'un mail en utf-8"""
    # Thanks to https://petermolnar.net/article/not-mime-email-python-3/
    for ill in [ "\n", "\r" ]:
        subject = subject.replace(ill, ' ')
    
    headers = {
        'Content-Type': 'text/html; charset=utf-8',
        'Content-Disposition': 'inline',
        'Content-Transfer-Encoding': '8bit',
        'From': mail_from,
        'To': mail_to,
        'Date': datetime.datetime.now().strftime('%a, %d %b %Y  %H:%M:%S %Z'),
        'X-Mailer': 'python/galilee_accounts',
        'Subject': subject
    }
    
    
    # create the message
    msg = ''
    for key, value in headers.items():
        msg += "%s: %s\n" % (key, value)
    # add contents
    msg += "\n%s\n"  % (content)
        
    s = smtplib.SMTP("localhost")
    s.sendmail(headers['From'], headers['To'], msg.encode("utf8"))
    s.quit()
        
def filtres(request):
    def ldap_escape(filtre):
        return filtre.replace('(', '\\(').replace(')', '\\)')
    
    """Outil de création de filtres pour les listes mail"""
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FiltresForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # liste des fonctions
            fonc_list = form.cleaned_data['fonction_stable'] + form.cleaned_data['fonction_anc']
            
            if len(fonc_list) == 0:
                filtre_res = '({})'.format(
                    ldap_escape(form.cleaned_data['struct'])
                )
            else:
                if len(fonc_list) == 1:
                    filtre_f = '({})'.format(ldap_escape(fonc_list[0]))
                else:
                    filtre_f = '(|{})'.format(''.join(
                        '({})'.format(ldap_escape(f)) for f in fonc_list
                    ))
                    
                filtre_res = '(&({}){})'.format(
                    ldap_escape(form.cleaned_data['struct']),
                    filtre_f
                )
            
            return render(request, 'accounts/filtres.html', {
                'form': form,
                'filtre_res': filtre_res,
            })

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FiltresForm()

    return render(request, 'accounts/filtres.html', {
        'form': form,
        'filtre_res': '',
    })    
