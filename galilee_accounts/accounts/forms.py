# -*- coding: utf-8 -*-
###############################################################################
#       accounts/forms.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2020-2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-accounts.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Formulaires de l'application de gestion des comptes utilisateurices de Galilée"""
import sys
import re
import pwd
import datetime
from unidecode import unidecode
from django import forms
from django.conf import settings
from django.contrib.auth.password_validation import validate_password, password_validators_help_text_html
from .models import AdhValidation, ExtValidation

sys.path.insert(0, settings.GALILEE_LDAP_PATH)
import galilee_jeito
import galilee_ldap
from ldap import SCOPE_SUBTREE

NOT_ALPHA_PORT = re.compile("[^a-zA-z0-9\-']")
UNIX_USERNAME = re.compile("[a-z_][a-z0-9_-]*[$]?")
NUM_ADH = re.compile("[0-9]{3,7}|SAL-[0-9]{3,6}")

class CreationForm(forms.Form):
    """Formulaire de création de compte pour des personnes déjà adhérent·es"""
    nom = forms.CharField(label='Nom de famille', max_length=100)
    prenom = forms.CharField(label='Prénom', max_length=100)
    date_naissance = forms.DateField(label='Date de naissance')
    
    numadh = forms.CharField(label="Numéro d'adhérent·e", max_length=10, required=False,
        help_text="Optionnel : à remplir si les éléments précédents ne permettent pas de retrouver ta fiche adhérent·e (pour les salarié·es ou services civiques non-adhérent·es, utilise ton identifiant SAL-*)")
    
    login = forms.CharField(label="Nom d'utilisateurice", max_length=32,
        help_text="À choisir, doit être en minuscules, ne commence pas par un chiffre. Peut contenir - et _. Les autres caractères spéciaux (@, ., accents, etc) sont interdits.")
    
    motdepasse = forms.CharField(label='Mot de passe', max_length=100,
                    widget=forms.PasswordInput, 
                    help_text = password_validators_help_text_html())
    mdpconfirm = forms.CharField(label='Confirmation du mot de passe',
                    max_length=100, widget=forms.PasswordInput)

    def clean_nom(self):
        """Transforme le champ nom : majuscule, remplace les accents et les
        espaces"""
        nom = self.cleaned_data.get('nom')
        # remove accents, capitalize
        nom = unidecode(nom).upper()
        # remove non alpha chars
        nom = NOT_ALPHA_PORT.sub('', nom)
        return nom
    
    def clean_prenom(self):
        """Transforme le champ prenom: majuscule, remplace les accents et les
        espaces"""
        prenom = self.cleaned_data.get('prenom')
        # remove accents, capitalize
        prenom = unidecode(prenom).upper()
        # remove non alpha chars
        prenom = NOT_ALPHA_PORT.sub('', prenom)
        return prenom
    
    def clean_numadh(self):
        """Vérifier que le numéro d'adhérent·e est correct"""
        numadh = self.cleaned_data.get('numadh')
        if numadh and not NUM_ADH.fullmatch(numadh):
            raise forms.ValidationError("Ce n'est pas un numéro d'adhérent·e correct (ni un identifiant SAL-* correct).")
        return numadh
        
    def clean_login(self):
        """Vérifie que ce login n'existe pas déjà dans le ldap, et est un
        identifiant unix valide.
        """
        login = self.cleaned_data.get('login')
        if len(login) < 4 :
            raise forms.ValidationError("Le nom d'utilisateurice doit faire au moins 4 caractères de long.")
        if not UNIX_USERNAME.fullmatch(login):
            raise forms.ValidationError("Le nom d'utilisateurice doit être en minuscule, ne doit pas commencer par un chiffre, et peut contenir - et _. Les accents et autres caractères spéciaux (@, ., etc) sont interdits.")
        gldap = settings.LDAP_CONNECTOR
        res = gldap.recherche_numadh('(uid={})'.format(login))
        if res:
            raise forms.ValidationError("Le nom d'utilisateurice choisit existe déjà.")
        unix_users = [u.pw_name for u in pwd.getpwall()]
        if login in unix_users:
            raise forms.ValidationError("Le nom d'utilisateurice choisit existe déjà.")
        return login

    def clean_motdepasse(self):
        """Vérifie que le mot de passe passe les validations de django"""
        motdepasse = self.cleaned_data.get('motdepasse')
        validate_password(motdepasse)
        return motdepasse

    def clean(self):
        """Vérification de l'intégrité du formulaire en entier.
            ici : vérifier que l'on retrouve bien la fiche adhérent·e de la 
            personne
            + que les mots de passe correspondent
            et que la personne n'a pas une interdiction de créer un compte
        """
        cleaned_data = super().clean()
        
        nom = cleaned_data.get('nom')
        prenom = cleaned_data.get('prenom')
        date_naissance = cleaned_data.get('date_naissance')
        login = cleaned_data.get('login')
        
        if nom is None or prenom is None or date_naissance is None or login is None:
            # un autre problème de validité s'est posé
            return
        
        # Vérifier que les mots de passe correspondent et != de login
        motdepasse = cleaned_data.get('motdepasse')
        mdpconfirm = cleaned_data.get('mdpconfirm')
        if motdepasse != mdpconfirm:
            raise forms.ValidationError("Les mots de passe ne correspondent pas.")
        
        if login in motdepasse:
            raise forms.ValidationError("Le mot de passe ne peut pas contenir le nom d'utilisateurice")
        
        gldap = settings.LDAP_CONNECTOR
        # essayer de trouver une personne qui a le nom/prénom/date2naissance
        res = gldap.recherche_numadh(
            '(&(sn={nom})(givenName={prenom})(schacDateOfBirth={ddn}))'.format(
                nom = cleaned_data['nom'],
                prenom = cleaned_data['prenom'],
                ddn = cleaned_data['date_naissance'].strftime('%d%m%Y')
            )
        )
        
        # sinon, essayer avec son num d'adh
        try:
            numadh = cleaned_data['numadh']
        except KeyError:
            numadh = None
        if len(res) != 1 and numadh:
            res = gldap.recherche_numadh('(&(cn={cn})(schacDateOfBirth={ddn}))'.format(
                cn = gldap.normalize_num_adh(numadh),
                ddn = cleaned_data['date_naissance'].strftime('%d%m%Y')
            ))
            if len(res) != 1:
                raise forms.ValidationError("Aucune fiche adhérent·e n'a été "
                    "trouvée avec ces informations.")
        else:    
            if len(res) < 1:
                raise forms.ValidationError("Aucune fiche adhérent·e n'a été "
                    "trouvée avec ces informations. Tu peux réessayer en " 
                    "fournissant un numéro d'adhérent·e.")
            elif len(res) > 1:
                raise forms.ValidationError("Plusieurs fiches adhérent·es ont été "
                    "trouvées avec ces informations. Donne ton numéro d'adhérent·e "
                    "pour qu'on l'on crée ton compte avec la bonne fiche.")
        
        numadh = res[0]
        # vérifier que cet adhérent·e n'a pas déjà un compte
        try:
            uid_data = gldap.adh_data(numadh, ['uid'])['uid']
        except KeyError:
            pass
        else:
            raise forms.ValidationError("Il y a déjà un compte utilisateurice "
                "associé avec cette fiche adhérent·e.")
        
        # vérifier que cette personne n'est pas empêchée de créer un compte
        try:
            pwdata = gldap.adh_data(numadh, ['userPassword'])
        except KeyError:
            pass
        else:
            if pwdata:
                pwhash = pwdata['userPassword'][0]
                if pwhash == '{SSHA}!':
                    raise forms.ValidationError("Impossible de créer un compte lié à cette fiche d'adhérent·e.")
        
        # vérifier que le login n'est pas déjà en attente, sauf si c'est pour
        # le même numadh
        adhvalid_list = AdhValidation.objects.filter(login=login)
        if len(adhvalid_list) > 0:
            for adhvalid in adhvalid_list:
                if adhvalid.numadh != numadh:
                    raise forms.ValidationError("Le nom d'utilisateurice choisit existe déjà.")
        extvalid_list = ExtValidation.objects.filter(login=login)
        if len(extvalid_list) > 0:
            raise forms.ValidationError("Le nom d'utilisateurice choisit existe déjà.")
        
        # on a trouvé le bon numadh
        cleaned_data['numadh'] = numadh
        
        # on vérifier que cet adhérent·e a bien une adresse mail
        try:
            mail_data = gldap.adh_data(numadh, ['mail'])['mail']
        except KeyError:
            raise forms.ValidationError("Aucune adresse e-mail n'est associé avec cet·te adhérent·e. Demande à ton/ta gestionnaire des adhésions de renseigner un mail dans ta fiche.")
                
        return cleaned_data
        

class CreationFormExt(forms.Form):
    """Formulaire de création de compte pour des personnes extérieures"""
    nom = forms.CharField(label='Nom de famille', max_length=100)
    prenom = forms.CharField(label='Prénom', max_length=100)
    fonction = forms.CharField(label="Fonction aux éclés", max_length=100)
    adresse_mail = forms.EmailField(label="Adresse e-mail")
    
    login = forms.CharField(label="Nom d'utilisateurice", max_length=32,
        help_text="À choisir, doit être en minuscules, ne commence pas par un chiffre. Peut contenir - et _. Les autres caractères spéciaux (@, ., accents, etc) sont interdits.")
    
    motdepasse = forms.CharField(label='Mot de passe', max_length=100,
                    widget=forms.PasswordInput, 
                    help_text = password_validators_help_text_html())
    mdpconfirm = forms.CharField(label='Confirmation du mot de passe',
                    max_length=100, widget=forms.PasswordInput)
    
    motivation = forms.CharField(label='Raison de la demande', max_length=500,
        widget=forms.Textarea,
        help_text=("Explique nous en quelques mots pourquoi tu demande un compte "
            "sur Galilée. N'hésite pas à préciser la structure dont tu "
            "es membre (groupe local, région, centre…)."))
        
    def clean_login(self):
        """Vérifie que ce login n'existe pas déjà dans le ldap, et est un
        identifiant unix valide.
        """
        login = self.cleaned_data.get('login')
        if len(login) < 4 :
            raise forms.ValidationError("Le nom d'utilisateurice doit faire au moins 4 caractères de long.")
        if not UNIX_USERNAME.fullmatch(login):
            raise forms.ValidationError("Le nom d'utilisateurice doit être en minuscule, ne doit pas commencer par un chiffre, et peut contenir - et _. Les accents et autres caractères spéciaux (@, ., etc) sont interdits.")
        gldap = settings.LDAP_CONNECTOR
        res = gldap.recherche_numadh('(uid={})'.format(login))
        if res:
            raise forms.ValidationError("Le nom d'utilisateurice choisit existe déjà.")
        unix_users = [u.pw_name for u in pwd.getpwall()]
        if login in unix_users:
            raise forms.ValidationError("Le nom d'utilisateurice choisit existe déjà.")
        return login

    def clean_motdepasse(self):
        """Vérifie que le mot de passe passe les validations de django"""
        motdepasse = self.cleaned_data.get('motdepasse')
        validate_password(motdepasse)
        return motdepasse

    def clean(self):
        """Vérification de l'intégrité du formulaire en entier.
        """
        cleaned_data = super().clean()
        login = cleaned_data.get('login')
        
        if login is None:
            # un autre problème de validation s'est posé
            return
        
        # Vérifier que les mots de passe correspondent et != de login
        motdepasse = cleaned_data.get('motdepasse')
        mdpconfirm = cleaned_data.get('mdpconfirm')
        if motdepasse != mdpconfirm:
            raise forms.ValidationError("Les mots de passe ne correspondent pas.")
        login = cleaned_data.get('login')
        if login in motdepasse:
            raise forms.ValidationError("Le mot de passe ne peut pas contenir le nom d'utilisateurice")
        
        # vérifier que le login n'est pas déjà en attente, sauf si c'est pour
        # le même mail
        adhvalid_list = AdhValidation.objects.filter(login=login)
        if len(adhvalid_list) > 0:
            raise forms.ValidationError("Le nom d'utilisateurice choisit existe déjà.")
        extvalid_list = ExtValidation.objects.filter(login=login)
        if len(extvalid_list) > 0:
            for extvalid in extvalid_list:
                if extvalid.adresse_mail != cleaned_data.get('adresse_mail'):
                    raise forms.ValidationError("Le nom d'utilisateurice choisit existe déjà.")
                        
        return cleaned_data

class ChMotDePasse(forms.Form):
    """Formulaire de changement de mot de passe"""
    adresse_mail = forms.EmailField(label="Adresse e-mail")
    
    numadh = forms.CharField(label="Numéro d'adhérent·e", max_length=10, required=False,
        help_text="Optionnel : à utiliser si ton mail correspond à plusieurs fiches adhérent·es.")
    
    motdepasse = forms.CharField(label='Mot de passe', max_length=100,
                    widget=forms.PasswordInput, 
                    help_text = password_validators_help_text_html())
    mdpconfirm = forms.CharField(label='Confirmation du mot de passe',
                    max_length=100, widget=forms.PasswordInput)
    
    def clean_numadh(self):
        """Vérifier que le numéro d'adhérent·e est correct"""
        numadh = self.cleaned_data.get('numadh')
        if numadh and not NUM_ADH.fullmatch(numadh):
            raise forms.ValidationError("Ce n'est pas un numéro d'adhérent·e correct.")
        return numadh

    def clean_motdepasse(self):
        """Vérifie que le mot de passe passe les validations de django"""
        motdepasse = self.cleaned_data.get('motdepasse')
        validate_password(motdepasse)
        return motdepasse

    def clean(self):
        """Vérification de l'intégrité du formulaire en entier.
        """
        cleaned_data = super().clean()
        gldap = settings.LDAP_CONNECTOR
        
        # récupérer le numadh
        try:
            numadh = cleaned_data['numadh']
        except KeyError:
            numadh = None
        
        if not numadh:
            res = gldap.recherche_numadh('(mail={})'.format(cleaned_data['adresse_mail']))
            if len(res) < 1:
                raise forms.ValidationError("Aucun compte correspondant à ce mail n'a été trouvé.")
        
            # pour chaque numadh, vérifier qu'il y a un compte utilisateurice
            numadh_list = []
            login_list = []
            for numadh in res:
                uid_data = gldap.adh_data(numadh, ['uid'])
                if uid_data:
                    numadh_list.append(numadh)
                    login_list.append(uid_data['uid'][0])
            
            if not numadh_list:
                raise forms.ValidationError("Aucun compte correspondant à ce mail n'a été trouvé.")
            
            if len(numadh_list) > 1:
                raise forms.ValidationError(
                    "Plusieurs fiches adhérent·es ont un compte lié à ce mail. "
                    "Merci de préciser le numéro d'adhérent·es de celle dont "
                    "tu veux changer le mot de passe : {}".format(
                        ', '.join(
                            "{} ({})".format(numadh, login_list[i])
                            for i, numadh in enumerate(numadh_list)
                        )
                    )
                )
            
            # c'est bon, on a un numadh !
            numadh = numadh_list[0]
            login = login_list[0]
        else:
            # on a déjà un numadh, on recherche le login
            try:
                uid_data = gldap.adh_data(numadh, ['uid'])
            except gldap.NumAdhInconnu:
                raise forms.ValidationError("Aucun compte correspondant à ce numéro d'adhérent·e n'a été trouvé (essaye sans préciser de numéro d'adhérent·e ?)")
            if uid_data:
                login = uid_data['uid'][0]
                numadh = gldap.normalize_num_adh(numadh)
            else:
                raise forms.ValidationError("Aucun compte correspondant à ce numéro d'adhérent·e n'a été trouvé (essaye sans préciser de numéro d'adhérent·e ?)")
        
        cleaned_data['numadh'] = numadh
        
        # vérifier que le compte n'est pas désactivé
        pwdata = gldap.adh_data(numadh, ['userPassword'])
        if pwdata:
            pwhash = pwdata['userPassword'][0]
            if pwhash == '{SSHA}!':
                raise forms.ValidationError("Ce compte est désactivé. Impossible de changer son mot de passe.")
        
        # Vérifier que les mots de passe correspondent et != de login
        motdepasse = cleaned_data.get('motdepasse')
        mdpconfirm = cleaned_data.get('mdpconfirm')
        if motdepasse != mdpconfirm:
            raise forms.ValidationError("Les mots de passe ne correspondent pas.")
        if login in motdepasse:
            raise forms.ValidationError("Le mot de passe ne peut pas contenir le nom d'utilisateurice")
        
        return cleaned_data

class FiltresData:
    """Données de structures/fonctions pour les filtres"""

    @classmethod
    def get_structures(cls):
        """Renvoie la liste des structures sous formes de choices django"""
        regions = []
        structures = []
        gldap = galilee_ldap.GalileeLDAP()
        # récupérer la liste des régions
        res = gldap.ldap_connexion.search_s(                                     
                gldap.filter_structure + gldap.bdn,                                
                SCOPE_SUBTREE,                                                   
                '(objectClass=organizationalUnit)',                      
                ['ou']
            )
        res = gldap._bytes_to_unicode(res)
        for dn, attr in res:
            name = attr['ou'][0]
            regions.append(name)
        
        regions.sort()
        # récupérer la liste des structures par région
        for region in regions:
            res = gldap.ldap_connexion.search_s(                                     
                gldap.filter_structure + gldap.bdn,                                
                SCOPE_SUBTREE,                                                   
                '(ou={})'.format(region),                      
                ['o']
            )
            res = gldap._bytes_to_unicode(res)
            # rendre la liste unique avec un set
            o_set = set()
            for dn, attr in res:
                try:
                    for o_item in attr['o']:
                        o_set.add(o_item)
                except KeyError:
                    # pas d'attr o
                    pass
            
            o_list = list(o_set)
            o_list.sort()
            
            if region == 'national':
                msg = "Toustes les adhérent·es du périmètre national"
            elif region == 'becours':
                msg = "Ancienne structure de bécours"
            elif region == 'guests':
                msg = "Comptes invité·es sur Galilée"
            elif region == 'midipy-futuraaee':
                msg = "Ancien·nes adhérent·es de Midi-py ayant un compte Galilée"
            else:
                msg = "Toute la région {}"
            
            if region != 'groups':
                structures.append(
                    (region,
                        [
                            ('ou:dn:={}'.format(region), msg.format(region))
                        ] + [('o={}'.format(o), o) for o in o_list if o != region]
                    )
                )
        
        return structures
        

    FONCTIONS_STABLE_CACHE = None
    FONCTIONS_ANCIENNES_CACHE = None
    CACHE_DATE = None

    @classmethod
    def _load_cache_fonctions(cls):
        """Vérifie s'il faut charger le cache des fonctions"""
        # vérifier la validité du cache
        now = datetime.date.today()
        if cls.CACHE_DATE == now:
            # le cache a déjà été rempli le jour même
            return
    
        # remplir le cache
        ja = galilee_jeito.JeitoAPI()
        (fonc_set, stable_fonc_set) = ja.fonctions()
        
        # cache des fonctions anciennes
        fonc_list = list(fonc_set)
        fonc_list.sort()
        cls.FONCTIONS_ANCIENNES_CACHE = [
            ('title={}'.format(f), f) for f in fonc_list
        ]
        
        # cache des fonctions stables
        
        # créer les fonctions joker
        fonc_jok_set = set()
        for f in stable_fonc_set:
            fonction_seule = f.split(' [')[0]
            fonc_jok_set.add(fonction_seule + '*')
        fonc_jok = list(fonc_jok_set)
        fonc_jok.sort()
        cls.FONCTIONS_STABLE_CACHE = [
            ('Fonctions joker',
                [('title={}'.format(f), "Toustes les {}".format(f)) for f in fonc_jok]
            )
        ]
        fonc_stables_list = list(stable_fonc_set)
        fonc_stables_list.sort()
        # par unité/échelon
        motif_unites = [
            ('Unité lutin·e', ['lutin']),
            ('Unité louvet·te', ['louvet']),
            ('Unité éclé·e', ['éclé', 'eclai']),
            ('Unité aîné·e', ['aîn']),
            ('Équipe de gestion et d\'animation (équipe de groupe)', ['équipe de gestion et d\'animation']),
            ('Équipe régionale', ['équipe régionale']),
            ('Formation', ['format', 'stag']),
            ('Activité adaptée', ['activité adaptée']),
        ]
        for nom_unite, motif_list in motif_unites:
            fonctions_unites = []
            # trouver les fonctions correspondantes à cette unité
            for motif in motif_list:
                for f in fonc_stables_list:
                    if motif in f.lower():
                        fonctions_unites.append(f)
            # les supprimer de la liste générale
            for f in fonctions_unites:
                fonc_stables_list.remove(f)
            fonctions_unites.sort()
            # ajouter ces fonctions 
            cls.FONCTIONS_STABLE_CACHE.append(
                (nom_unite, [
                    ('title={}'.format(f), f) for f in fonctions_unites
                ])
            )
        # ajouter les autres fonctions
        cls.FONCTIONS_STABLE_CACHE.append(
            ('Autres fonctions', [
                ('title={}'.format(f), f) for f in fonc_stables_list
            ])
        )
        
        # mise à jour de la date du cache
        cls.CACHE_DATE = now
    
    @classmethod
    def get_fstables(cls):
        """Renvoie la liste des fonctions stables"""
        cls._load_cache_fonctions()
        return cls.FONCTIONS_STABLE_CACHE
    
    @classmethod
    def get_fanciennes(cls):
        """Renvoie la liste des fonctions anciennes"""
        cls._load_cache_fonctions()
        return cls.FONCTIONS_ANCIENNES_CACHE

class FiltresForm(forms.Form):
    """Formulaire de création de filtres de listes e-mail"""
    struct = forms.ChoiceField(label="Structure")
    fonction_stable = forms.MultipleChoiceField(label="Fonctions stables", widget=forms.CheckboxSelectMultiple, required=False)
    fonction_anc = forms.MultipleChoiceField(label="Fonctions non stables", widget=forms.CheckboxSelectMultiple, required=False)

    def __init__(self, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        self.fields['struct'].choices = FiltresData.get_structures()
        self.fields['fonction_stable'].choices = FiltresData.get_fstables()
        self.fields['fonction_anc'].choices = FiltresData.get_fanciennes()

