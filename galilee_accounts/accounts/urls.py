# -*- coding: utf-8 -*-
###############################################################################
#       accounts/urls.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2020-2022, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-accounts.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""URL de l'application de gestion des comptes utilisateurices de Galilée"""


from django.urls import path

from . import views

urlpatterns = [
    path('creation/', views.creation, name='creation'),
    path('creation/demande_en_cours/<str:obs_mail>/', views.demande_en_cours, name='demande_en_cours'),
    path('creation/confirm/<uuid:code>/', views.adh_confirm, name='adh_confirm'),
    path('creation_ext/', views.creation_ext, name='creation_ext'),
    path('creation_ext/demande_en_cours/', views.demande_en_cours_ext, name='demande_en_cours_ext'),
    path('creation_ext/confirm/<uuid:code>/', views.ext_confirm, name='ext_confirm'),
    path('creation_ext/refus/<uuid:code>/', views.ext_refus, name='ext_refus'),
    path('mail_invalide/<str:adresse_mail>/', views.mail_invalide, name='mail_invalide'),
    path('changer_mdp/', views.changer_mdp, name='changer_mdp'),
    path('changer_mdp/demande_en_cours/', views.demande_en_cours_mdp, name='demande_en_cours_mdp'),
    path('changer_mdp/confirm/<uuid:code>/', views.mdp_confirm, name='mpd_confirm'),
    path('filtres/', views.filtres, name='filtres'),
]
