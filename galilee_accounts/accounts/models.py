# -*- coding: utf-8 -*-
###############################################################################
#       accounts/models.py
#       
#       Copyright © 2009-2020, Émile Decorsière <al@md2t.eu>
#       Copyright © 2020-2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of galilee-accounts.
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <https://www.gnu.org/licenses/>.
#       
###############################################################################
"""Formulaires de l'application de gestion des comptes utilisateurices de Galilée"""

from django import forms
from django.contrib import admin
from django.db import models
from django.conf import settings

class AdhValidation(models.Model):
    """Demande de validation d'un·e adhérent·e"""
    numadh = models.CharField("Numéro d'adhérent·e", max_length=10)
    adresse_mail = models.CharField("Adresse mail", max_length = 100)
    login = models.CharField("Nom d'utilisateurice", max_length=36)
    motdepasse = models.CharField("Mot de passe", max_length=100)
    confirmation_code = models.CharField("Code de confirmation", max_length=33, unique=True)
    date_de_demande = models.DateTimeField("Date de demande", auto_now_add=True)
    class Meta:
        verbose_name = "Demande de validation d'adhérent·e"

@admin.register(AdhValidation)
class AdhValidationAdmin(admin.ModelAdmin):
    """Infos d'administration de AdhValidation"""
    date_hierarchy = 'date_de_demande'
    readonly_fields = ('numadh', 'adresse_mail', 'login', 'motdepasse', 'confirmation_code', 'date_de_demande')
    list_display = ('numadh', 'login', 'date_de_demande')

class ExtValidation(models.Model):
    """Demande de validation d'un·e extérieur·e"""
    nom = models.CharField("Nom de famille", max_length=100)
    prenom = models.CharField("Prénom", max_length=100)
    fonction = models.CharField("Fonction aux éclés", max_length=100)
    adresse_mail = models.CharField("Adresse e-mail", max_length=100)
    login = models.CharField("Nom d'utilisateurice", max_length=36)
    motdepasse = models.CharField("Mot de passe", max_length=100)
    confirmation_code = models.CharField("Code de confirmation", max_length=33, unique=True)
    motivation = models.CharField("Raison de la demande", max_length=500)
    date_de_demande = models.DateTimeField("Date de demande", auto_now_add=True)
    class Meta:
        verbose_name = "Demande de validation d'extérieur·e"

@admin.register(ExtValidation)
class ExtValidationAdmin(admin.ModelAdmin):
    """Infos d'administration de ExtValidation"""
    date_hierarchy = 'date_de_demande'
    readonly_fields = ('nom', 'prenom', 'fonction', 'adresse_mail', 'login', 'motdepasse', 'confirmation_code', 'motivation', 'date_de_demande')
    list_display = ('nom', 'prenom', 'login', 'date_de_demande') 
    formfield_overrides = {
        ExtValidation.motivation: {'widget': forms.Textarea},
    }

class GuestLastNumAdh(models.Model):
    """Dernier numéro d'adhérent·e factice attribué à une personne extérieure"""
    numadh = models.IntegerField(default=settings.GUEST_START_NUMADH)
    class Meta:
        verbose_name = "Dernier numéro d'adhérent·e factice attribué à un·e extérieur·e."
    
@admin.register(GuestLastNumAdh)
class GuestLastNumAdhAdmin(admin.ModelAdmin):
    """Infos d'administration de GuestLastNumAdh"""
    pass

class MdpValidation(models.Model):
    """Demande de validation d'un changement de mot de passe"""
    adresse_mail = models.CharField("Adresse e-mail", max_length=100)
    numadh = models.CharField("Numéro d'adhérent·e", max_length=10)    
    motdepasse = models.CharField("Mot de passe", max_length=100)
    confirmation_code = models.CharField("Code de confirmation", max_length=33, unique=True)
    date_de_demande = models.DateTimeField("Date de demande", auto_now_add=True)
    class Meta:
        verbose_name = "Demande de changement de mot de passe"

@admin.register(MdpValidation)
class MdpValidationAdmin(admin.ModelAdmin):
    """Infos d'administration de MdpValidation"""
    date_hierarchy = 'date_de_demande'
    readonly_fields = ('adresse_mail', 'numadh', 'motdepasse', 'confirmation_code', 'date_de_demande')
    list_display = ('adresse_mail', 'numadh', 'date_de_demande') 
